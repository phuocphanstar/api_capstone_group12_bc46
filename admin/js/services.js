const BASE_URL= "https://648f143b75a96b664444aba0.mockapi.io/product";
var prService = {
    getList: function(){
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    delete: function(id){
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        });
    },
    create: function(product){
        return axios({
            url: BASE_URL,
            method: "POST",
            data: product,
        });
    },
    getById: function(id){
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        });
    },
    update: function(id,product){
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: product,
        });

    }
}