function showMessage(id, message) {
    document.getElementById(id).innerHTML = message;
    document.getElementById(id).style.display = "block"; 
  }
  function kiemTraTrong(value, id) {
    if (value == "" || value == -1) {
      showMessage(id, "Vui lòng nhập dữ liệu!");
      return false;
    } else {
      showMessage(id, "");
      return true;
    }
  }
  function kiemTraLaSo(value,id){
    if(isNaN(value)){
        //Nếu value là số thì trả ra true, k là số thì trả ra false
        showMessage(id,"Vui lòng nhập số");
        return false;
    }
    else {
        showMessage(id,"");
        return true;
    }

}

  function kiemTraForm(sp){
    return (kiemTraTrong(sp.name,"tbname") & (kiemTraTrong(sp.price,"tbprice")&&kiemTraLaSo(sp.price,"tbprice"))&kiemTraTrong(sp.img,"tbimg")&kiemTraTrong(sp.type,"tbtype"))
  }