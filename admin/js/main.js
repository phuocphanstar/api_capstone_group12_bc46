function success() {
  swal("Nice!", "Action completed!", "success");
}
var idUpdate = null;
//lấy dssp từ api và render
function fetchSPList() {
  batLoading();
  prService
    .getList()
    .then((res) => {
      renderDSSP(res.data);
      tatLoading();
    })
    .catch((err) => {
      console.log(err);
      tatLoading();
    });
}
fetchSPList();

function themSP() {
  var sp = layThongTinTuForm();
  var isValid = kiemTraForm(sp);
  if (isValid) {
    batLoading();
    prService
      .create(sp)
      .then((res) => {
        success();
        reSetForm();
        fetchSPList();
        $('#exampleModal').modal('hide')
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

function xoaSP(id) {
  batLoading();
  prService
    .delete(id)
    .then((res) => {
      success();
      fetchSPList();
      reSetForm();
    })
    .catch((err) => {
      console.log(err);
    });
}
function suaSP(id) {
  idUpdate = id;
  prService
    .getById(id)
    .then((res) => {
      showThongTinLenForm(res.data);
      $("#exampleModal").modal("show");
    })
    .catch((err) => {
      console.log(err);
    });
}

function capNhatSP() {
  batLoading();
  var sp = layThongTinTuForm();
  prService
    .update(idUpdate, sp)
    .then((res) => {
      success();
      fetchSPList();
      tatLoading();
      $("#exampleModal").modal("hide");
      reSetForm();
    })
    .catch((err) => {
      tatLoading();
    });
}
function locPhanLoai(sp){
  prService.getList()
  .then((res) => {
    let ress = res.data;
    if (sp.value === "-1") {
      renderDSSP(ress);
    } 
    else {
      let newArr = ress.filter((item) => item.type == sp.value);
      renderDSSP(newArr);
    }
  })
  .catch((err) => {});
}

function sapXepTang(){
  prService.getList()
  .then((res) => {
    let ress = res.data;
      let newArr = ress.sort((a,b) => b.price - a.price);
      renderDSSP(newArr);
    }
  )
  .catch((err) => {});

}
function sapXepGiam(){
  prService.getList()
  .then((res) => {
    let ress = res.data;
      let newArr = ress.sort((a,b) => a.price - b.price);
      renderDSSP(newArr);
    }
  )
  .catch((err) => {});

}


function reSetForm() {
  document.getElementById("formPhone").reset();
}

